import {combineReducers} from "redux";
import Articles from "../article/ArticlesReducer";

// Combine all reducers into one root reducer
export const RootReducer = combineReducers({
    Articles
});
