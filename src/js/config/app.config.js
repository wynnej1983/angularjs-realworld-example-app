import thunk from 'redux-thunk';
import authInterceptor from './auth.interceptor'
import {RootReducer} from './reducers'

function AppConfig($httpProvider, $stateProvider, $locationProvider, $urlRouterProvider, $ngReduxProvider) {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);

  $ngReduxProvider.createStoreWith(RootReducer, [thunk], [window.__REDUX_DEVTOOLS_EXTENSION__()]);

  /*
    If you don't want hashbang routing, uncomment this line.
    Our tutorial will be using hashbang routing though :)
  */
  // $locationProvider.html5Mode(true);

  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'layout/app-view.html',
    resolve: {
      auth: function(User) {
        return User.verifyAuth();
      }
    }
  });

  $urlRouterProvider.otherwise('/');

}

export default AppConfig;
