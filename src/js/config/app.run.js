function AppRun(AppConstants, $rootScope, $ngRedux, $timeout) {
  'ngInject';

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.setPageTitle(toState.title);
  });

  // Helper method for setting the page's title
  $rootScope.setPageTitle = (title) => {
    $rootScope.pageTitle = '';
    if (title) {
      $rootScope.pageTitle += title;
      $rootScope.pageTitle += ' \u2014 ';
    }
    $rootScope.pageTitle += AppConstants.appName;
  };

  //To reflect state changes when disabling/enabling actions via the monitor
  //there is probably a smarter way to achieve that
  $ngRedux.subscribe(() => {
      $timeout(() => {$rootScope.$apply(() => {})}, 100);
  });
}

export default AppRun;
