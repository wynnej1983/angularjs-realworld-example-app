export const ADD_RATING = "ADD_RATING";

export function addRating(articleId, rating) {
    return {
        type: ADD_RATING,
        rating,
        articleId,
    };
}
