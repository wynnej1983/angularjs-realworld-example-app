import { ADD_RATING } from "./ArticlesActions";

const initialState = {};

const ArticlesReducer = (state = initialState, action) => {
    const {articleId, rating} = action;
    switch (action.type) {

    case ADD_RATING :
        return {
            ...state,
            [articleId]: rating,
        };

    default:
        return state;
    }
};

/* Selectors */

// Get all ratings
export const getRating = state => id => state.Articles[id];

// Export Reducer
export default ArticlesReducer;
