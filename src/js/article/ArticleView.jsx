import React from 'react'
import { connect } from "react-redux";
import {PropTypes} from 'prop-types';
import flowRight from "lodash.flowright";

import {mapNgReduxToStore} from '../helpers/mapNgReduxToStore';
import {getRating} from './ArticlesReducer';
import Banner from '../components/article-helpers/Banner.jsx';
import ArticleContent from '../components/article-helpers/ArticleContent.jsx';

export const ArticleView = ({
  article,
  comments = [],
  User,
  Articles,
}) => {
  const user = User.current;
  const canModify = user && user.username === article.author.username;

  const handleFavorite = React.useCallback(() => {
    // TODO
    console.log(`Article ${article.slug} favorited`);
  }, []);

  const handleFollow = React.useCallback(() => {
    // TODO
    console.log(`Article ${article.slug} followed`);
  }, []);

  const handleDelete = React.useCallback(() => {
    // TODO
    console.log(`Article ${article.slug} deleted`);
  }, []);

  const handleDeleteComment = React.useCallback(() => {
    // TODO
    console.log(`Article comment ${article.slug} deleted`);
  }, []);

  return (
    <div>
      <Banner
        article={{...article, canModify}}
        onFollow={handleFollow}
        onFavorite={handleFavorite}
        onDelete={handleDelete}
      />
      <ArticleContent
        article={{...article, canModify}}
        comments={comments}
        user={user}
        onFollow={handleFollow}
        onFavorite={handleFavorite}
        onDelete={handleDelete}
        onDeleteComment={handleDeleteComment}
      />
    </div>
  )
};

ArticleView.propTypes = {
  article: PropTypes.object.isRequired,
  comments: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    body: PropTypes.string.isRequired,
    author: PropTypes.object.isRequired,
  })),
}

export default ArticleView;
// const mapStateToProps = (state, ownProps) => {
//     return {
//         rating: getRating(state)(ownProps.slug),
//     }
// };
// const mapDispatchToProps = (dispatch) => ({})

// export default flowRight(
//   mapNgReduxToStore,
//   connect(mapStateToProps, mapDispatchToProps)
// )(ArticleView);
