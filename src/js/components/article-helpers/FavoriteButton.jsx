import React from 'react'
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

export const FavoriteButton = ({
  article,
  children,
  isSubmitting,
  onSubmit,
}) => {
  const btnClass = classNames('btn btn-sm', {
    disabled: isSubmitting,
    'btn-outline-secondary': !article.favorited,
    'btn-primary': article.favorited,
  })
  return (
    <button
      className={btnClass}
      onClick={onSubmit}>
      <i className="ion-heart"></i> {children}
    </button>
  )
};

FavoriteButton.propTypes = {
  article: PropTypes.object.isRequired,
}

export default FavoriteButton;
