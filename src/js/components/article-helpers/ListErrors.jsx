import React from 'react'
import {PropTypes} from 'prop-types';
import ArticleActions from './ArticleActions.jsx';

export const ListErrors = ({
  errors,
}) => {
  return (
    <ul className="error-messages">
        {errors.map(error => (
          <li>
            {error}
          </li>
        ))}
    </ul>
  )
};

ListErrors.propTypes = {
  errors: PropTypes.arrayOf(PropTypes.string).isRequired,
}

export default ListErrors;
