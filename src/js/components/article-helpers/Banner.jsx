import React from 'react'
import {PropTypes} from 'prop-types';
import ArticleActions from './ArticleActions.jsx';

export const Banner = ({
  article,
  onFollow,
  onFavorite,
  onDelete,
}) => {
  return (
    <div className="banner">
      <div className="container">
        <h1>{article.title}</h1>
        <div className="article-meta">
          <ArticleActions
            article={article}
            onFollow={onFollow}
            onFavorite={onFavorite}
            onDelete={onDelete}
          />
        </div>
      </div>
    </div>
  )
};

Banner.propTypes = {
  article: PropTypes.object.isRequired,
}

export default Banner;
