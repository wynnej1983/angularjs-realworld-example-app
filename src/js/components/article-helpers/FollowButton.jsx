import React from 'react'
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

export const FollowButton = ({
  user,
  isSubmitting,
  onSubmit,
}) => {
  const btnClass = classNames('btn btn-sm action-btn', {
    disabled: isSubmitting,
    'btn-outline-secondary': !user.following,
    'btn-secondary': user.following,
  })
  return (
    <button
      className={btnClass}
      onClick={onSubmit}>
      <i className="ion-plus-round"></i>
      &nbsp;
      {user.following ? 'Unfollow' : 'Follow'} {user.username}
    </button>
  )
};

FollowButton.propTypes = {
  user: PropTypes.object.isRequired,
}

export default FollowButton;
