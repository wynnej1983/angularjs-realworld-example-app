import React from 'react'
import {PropTypes} from 'prop-types';
import ArticleActions from './ArticleActions.jsx';
import ArticleComment from './ArticleComment.jsx';
import ListErrors from './ListErrors.jsx';

export const ArticleContent = ({
  article,
  user,
  comments,
  onComment,
  onFollow,
  onFavorite,
  onDelete,
  onDeleteComment,
}) => {
  const [commentForm, setCommentForm] = React.useState({
    isSubmitting: false,
    body: '',
    errors: [],
  })

  return (
    <div className="container page">
      <div className="row article-content">
        <div className="col-xs-12">
          <div dangerouslySetInnerHTML={{__html: article.body}} />
          <ul className="tag-list">
            {article.tagList.map(tag => (
              <li className="tag-default tag-pill tag-outline">
                {tag}
              </li>
            ))}
          </ul>
        </div>
      </div>
      <hr />
      <div className="article-actions">
        <ArticleActions
          article={article}
          onFollow={onFollow}
          onFavorite={onFavorite}
          onDelete={onDelete}
        />
      </div>

      <div className="row">
        <div className="col-xs-12 col-md-8 offset-md-2">

          {user &&
            <div>
              {commentForm.errors &&
                <ListErrors errors={commentForm.errors} />
              }
              <form className="card comment-form" onSubmit={onComment}>
                <fieldset disabled={commentForm.isSubmitting}>
                  <div className="card-block">
                    <textarea className="form-control"
                      placeholder="Write a comment..."
                      rows="3"
                      value={commentForm.body}
                      onChange={evt => setCommentForm({body: evt.target.value})}>
                    </textarea>
                  </div>
                  <div className="card-footer">
                    <img src={user.image} className="comment-author-img" />
                    <button className="btn btn-sm btn-primary" type="submit">
                    Post Comment
                    </button>
                  </div>
                </fieldset>
              </form>
            </div>
          }

          {!user &&
            <div>
              <a href="#!/login">Sign in</a> or <a href="#!/register">sign up</a> to add comments on this article.
            </div>
          }

          {comments.map((comment, idx) => {
            const canModify = user && user.username === comment.author.username;
            return (
              <ArticleComment
                comment={comment}
                canModify={canModify}
                onDelete={() => onDeleteComment(comment.id, idx)}>
              </ArticleComment>
            );
          })}

        </div>
      </div>

    </div>
  )
};

ArticleContent.propTypes = {
  article: PropTypes.object.isRequired,
}

export default ArticleContent;
