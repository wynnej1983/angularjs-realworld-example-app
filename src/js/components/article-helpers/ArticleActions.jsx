import React from 'react'
import {PropTypes} from 'prop-types';
import ArticleMeta from './ArticleMeta.jsx';
import FollowButton from './FollowButton.jsx';
import FavoriteButton from './FavoriteButton.jsx';

export const ArticleActions = ({
  article,
  isDeleting,
  onFollow,
  onFavorite,
  onDelete,
}) => {
  return (
    <ArticleMeta article={article}>
      {article.canModify &&
        <span>
          <a className="btn btn-sm btn-outline-secondary"
            href={`#!/editor/${article.slug}`}>
            <i className="ion-edit"></i> Edit Article
          </a>

          <button className="btn btn-sm btn-outline-danger"
            disabled={isDeleting}
            onClick={onDelete}>
            <i className="ion-trash-a"></i> Delete Article
          </button>
        </span>
      }

      {!article.canModify &&
        <span>
          <FollowButton user={article.author} onSubmit={onFollow}></FollowButton>
          <FavoriteButton article={article} onSubmit={onFavorite}>
            {article.favorited ? 'Unfavorite' : 'Favorite'} Article <span className="counter">({article.favoritesCount})</span>
          </FavoriteButton>
        </span>
      }
    </ArticleMeta>
  )
};

ArticleActions.propTypes = {
}

export default ArticleActions;
