import React from 'react'
import {PropTypes} from 'prop-types';

export const ArticleComment = ({
  comment,
  canModify,
  onDelete,
}) => {
  return (
    <div class="card">
      <div class="card-block">
        <p class="card-text">{comment.body}</p>
      </div>
      <div class="card-footer">
        <a class="comment-author" href={`#!/@${comment.author.username}`}>
          <img src={comment.author.image} class="comment-author-img" />
        </a>
        &nbsp;
        <a class="comment-author" href={`#!/@${comment.author.username}`}>
          {comment.author.username}
        </a>
        <span class="date-posted">
          {comment.createdAt}
        </span>
        {canModify &&
            <span class="mod-options">
              <i class="ion-trash-a" onClick={onDelete}></i>
            </span>
        }
      </div>
    </div>
  )
};

ArticleComment.propTypes = {
  comment: PropTypes.object.isRequired,
}

export default ArticleComment;
