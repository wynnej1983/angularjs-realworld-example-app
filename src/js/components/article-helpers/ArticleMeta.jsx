import React from 'react'
import {PropTypes} from 'prop-types';

export const ArticleMeta = ({
  article,
  children
}) => {
  return (
    <div className="article-meta">
      <a href={`#!@${article.author.username}`}>
        <img src={article.author.image} />
      </a>

      <div className="info">
        <a className="author"
          href={`#!@${article.author.username}`}>
          {article.author.username}
        </a>
        <span className="date">
          {article.createdAt}
        </span>
      </div>

      {children}
    </div>
  )
};

ArticleMeta.propTypes = {
  article: PropTypes.object.isRequired,
}

export default ArticleMeta;
