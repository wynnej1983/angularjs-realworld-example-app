import React from 'react';
import {PropTypes} from 'prop-types';

// This HOC simply renames $ngRedux to store (because this is used with react2angular)
export const mapNgReduxToStore = WrappedComponent => {
  const MappedComponent = ({ $ngRedux, ...rest }) => {
    return (<WrappedComponent {...rest} store={$ngRedux} />);
  };
  MappedComponent.displayName = `WithStore(${WrappedComponent.displayName})`;
  MappedComponent.propTypes = {
    $ngRedux : PropTypes.object,
  };
  return MappedComponent;
};
